package com.company;

public class Balok implements Operasi1, Operasi2{
    public double p, l, t;

    @Override
    public double luas() {
        return  2 * ((p*l) + (p*t) * (l*t));
    }

    @Override
    public double keliling() {
        return 4 * (p+l+t);
    }

    @Override
    public void volume() {
        System.out.println("Volume balok : "+ (p*l*t) );
    }
}

