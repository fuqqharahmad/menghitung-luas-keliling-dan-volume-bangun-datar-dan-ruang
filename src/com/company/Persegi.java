package com.company;

public class Persegi implements Operasi1 {
    public double s;

    @Override
    public double luas() {
        return s * s;
    }

    @Override
    public double keliling() {
        return 4 * s;
    }
}