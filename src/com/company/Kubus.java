package com.company;

public class Kubus implements Operasi1, Operasi2{
    public double s;

    @Override
    public double luas() {
        return 6 * (s*s);
    }

    @Override
    public double keliling() {
        return 12 * s;
    }

    @Override
    public void volume() {
        System.out.println("Volume Kubus : "+ (s * s * s));
    }
}