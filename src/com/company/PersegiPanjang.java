package com.company;

public class PersegiPanjang implements Operasi1{
    public double p,l;

    @Override
    public double luas() {
        return p * l;
    }

    @Override
    public double keliling() {
        return 2 * (p + l);
    }
}