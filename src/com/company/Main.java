package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Persegi persegi = new Persegi();
        PersegiPanjang persegiPanjang = new PersegiPanjang();
        Balok balok = new Balok();
        Kubus kubus = new Kubus();
        LayangLayang LayangLayang = new LayangLayang();
        BelahKetupat BelahKetupat = new BelahKetupat();
        Scanner input = new Scanner(System.in);
        boolean loop = true;
        while (loop){
            System.out.println("Pilih bangun :");
            System.out.println("\t 1. Persegi");
            System.out.println("\t 2. Persegi Panjang");
            System.out.println("\t 3. Balok");
            System.out.println("\t 4. Kubus");
            System.out.println("\t 5. Layang-Layang");
            System.out.println("\t 6. Belah Ketupat");
            System.out.println("\t 7. Exit");
            System.out.print("Masukan pilihan : ");
            int choice = input.nextInt();
            switch(choice){
                case 1 :
                    System.out.print("Masukan sisi : ");
                    persegi.s = input.nextDouble();
                    System.out.println("Pilih operasi : ");
                    System.out.println("\t 1. Luas");
                    System.out.println("\t 2. Keliling");
                    System.out.print("Masukan pilihan : ");
                    choice = input.nextInt();
                    switch (choice){
                        case 1 :
                            System.out.println("Luas Persegi : "+ persegi.luas());
                            break;
                        case 2 :
                            System.out.println("Keliling Persegi : "+ persegi.keliling());
                            break;
                        default:
                            System.out.println("INVALID INPUT!!");
                            loop = false;
                            break;
                    }
                    break;

                case 2 :
                    System.out.print("Masukan panjang : ");
                    persegiPanjang.p = input.nextDouble();
                    System.out.print("Masukan lebar : ");
                    persegiPanjang.l = input.nextDouble();
                    System.out.println("Pilih operasi : ");
                    System.out.println("\t 1. Luas");
                    System.out.println("\t 2. Keliling");
                    System.out.print("Masukan pilihan : ");
                    choice = input.nextInt();
                    switch (choice){
                        case 1 :
                            System.out.println("Luas Persegi Panjang: "+ persegiPanjang.luas());
                            break;
                        case 2 :
                            System.out.println("Keliling Persegi Panjang: "+ persegiPanjang.keliling());
                            break;
                        default:
                            System.out.println("INVALID INPUT!!");
                            loop = false;
                            break;
                    }
                    break;

                case 3 :
                    System.out.print("Masukan panjang : ");
                    balok.p = input.nextDouble();
                    System.out.print("Masukan lebar : ");
                    balok.l = input.nextDouble();
                    System.out.print("Masukan tinggi : ");
                    balok.t = input.nextDouble();
                    System.out.println("Pilih operasi : ");
                    System.out.println("\t 1. Luas");
                    System.out.println("\t 2. Keliling");
                    System.out.println("\t 3. Volume");
                    System.out.print("Masukan pilihan : ");
                    choice = input.nextInt();
                    switch (choice){
                        case 1 :
                            System.out.println("Luas Persegi Panjang: "+ balok.luas());
                            break;
                        case 2 :
                            System.out.println("Keliling Persegi Panjang: "+ balok.keliling());
                            break;
                        case 3 :
                            balok.volume();
                            break;
                        default:
                            System.out.println("INVALID INPUT!!");
                            loop = false;
                            break;
                    }
                    break;

                case 4 :
                    System.out.println("Masukan sisi : ");
                    kubus.s = input.nextDouble();
                    System.out.println("Pilih operasi : ");
                    System.out.println("\t 1. Luas");
                    System.out.println("\t 2. Keliling");
                    System.out.println("\t 3. Volume");
                    System.out.print("Masukan pilihan : ");
                    choice = input.nextInt();
                    switch (choice){
                        case 1 :
                            System.out.println("Luas Persegi Panjang: "+ kubus.luas());
                            break;
                        case 2 :
                            System.out.println("Keliling Persegi Panjang: "+ kubus.keliling());
                            break;
                        case 3 :
                            kubus.volume();
                            break;
                        default:
                            System.out.println("INVALID INPUT!!");
                            loop = false;
                            break;
                    }
                    break;

                case 5 :
                    System.out.println("Masukan diagonal 1: ");
                    LayangLayang.d1 = input.nextDouble();
                    System.out.println("Masukan diagonal 2: ");
                    LayangLayang.d2 = input.nextDouble();
                    System.out.print("Masukan panjang : ");
                    balok.p = input.nextDouble();
                    System.out.print("Masukan lebar : ");
                    balok.l = input.nextDouble();
                    System.out.println("Pilih operasi : ");
                    System.out.println("\t 1. Luas");
                    System.out.println("\t 2. Keliling");
                    System.out.print("Masukan pilihan : ");
                    choice = input.nextInt();
                    switch (choice){
                        case 1 :
                            System.out.println("Luas Layang Layang: "+ LayangLayang.luas());
                            break;
                        case 2 :
                            System.out.println("Keliling Layang Layang: "+ LayangLayang.keliling());
                            break;
                        case 3 :
                            kubus.volume();
                            break;
                        default:
                            System.out.println("INVALID INPUT!!");
                            loop = false;
                            break;
                    }
                    break;

                case 6 :
                    System.out.println("Masukan diagonal 1: ");
                    LayangLayang.d1 = input.nextDouble();
                    System.out.println("Masukan diagonal 2: ");
                    LayangLayang.d2 = input.nextDouble();
                    System.out.println("Masukan sisi : ");
                    kubus.s = input.nextDouble();
                    System.out.println("Pilih operasi : ");
                    System.out.println("\t 1. Luas");
                    System.out.println("\t 2. Keliling");
                    System.out.print("Masukan pilihan : ");
                    choice = input.nextInt();
                    switch (choice){
                        case 1 :
                            System.out.println("Luas Belah Ketupat: "+ BelahKetupat.luas());
                            break;
                        case 2 :
                            System.out.println("Keliling Belah Ketupat: "+ BelahKetupat.keliling());
                            break;
                        case 3 :
                            kubus.volume();
                            break;
                        default:
                            System.out.println("INVALID INPUT!!");
                            loop = false;
                            break;
                    }
                    break;

                case 7 :
                    System.out.println("THXX!!!");
                    loop = false;
                    break;

                default:
                    System.out.println("INVALID INPUT!!!");
                    loop = false;
                    break;
            }
        }
    }
}
